package cs271.lab.list;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestPerformance {
  /*
      ArrayList and LinkedList are very similar to use.
      Their main difference is their implementation which causes different performance for different operations.
  */
  private final int SIZE = 10000;
  private final int REPS = 1000000;
  private List<Integer> arrayList;
  private List<Integer> linkedList;
  private long startTime;
  private long endTime;

  // isolating the logic of listAddRemove test
  private Consumer<List<Integer>> listAddRemove =
      list -> {
        for (int r = 0; r < REPS; r++) {
          list.add(0, 77);
          list.remove(0);
        }
      };

  // isolating the logic of listAccess test
  private Consumer<List<Integer>> listAccess =
      list -> {
        long sum = 0;
        for (int r = 0; r < REPS; r++) {
          sum += list.get(r % SIZE);
        }
      };

  // accepts a consumer (test logic) and a list on which we want to run test
  private long listPerformance(Consumer<List<Integer>> consumer, List<Integer> list) {
    startTime = System.currentTimeMillis();
    // invoking the lambda
    consumer.accept(list);
    endTime = System.currentTimeMillis();
    return endTime - startTime;
  }

  @Before
  public void setUp() throws Exception {
    arrayList = new ArrayList<Integer>(SIZE);
    linkedList = new LinkedList<Integer>();
    for (int i = 0; i < SIZE; i++) {
      arrayList.add(i);
      linkedList.add(i);
    }
  }

  @After
  public void tearDown() throws Exception {
    arrayList = null;
    linkedList = null;
  }

  @Test
  public void testListAddRemove() {
    /*
      LinkedList is implemented as a double linked list.
      Its performance on add and remove is better than Arraylist, but worse on get and set methods.
      So in this case LinkedList performs much better than ArrayList
    */

    assertTrue(
        listPerformance(listAddRemove, linkedList) < listPerformance(listAddRemove, arrayList));
  }

  @Test
  public void testListAccess() {
    /*
      ArrayList is implemented as a resizable array.
      Its elements can be accessed directly by using the get and set methods, since ArrayList is essentially an array.
      So in this case ArrayList performs much better than LinkedList
    */
    assertTrue(listPerformance(listAccess, linkedList) > listPerformance(listAccess, arrayList));
  }
}
