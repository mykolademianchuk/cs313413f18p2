Note: Refactored to DRY using Java 8 syntax

1) //also try with a LinkedList - does it make any difference?

    ArrayList and LinkedList are very similar to use.
    Their main difference is their implementation which causes different performance for different operations.
    ArrayList is implemented as a resizable array.
    Its elements can be accessed directly by using the get and set methods, since ArrayList is essentially an array.
    LinkedList is implemented as a double linked list.
    Its performance on add and remove is better than ArrayList, but worse on get and set methods.

2) list.remove(5); // what does this method do?

    It removes the element with index 5 (6th in the list, which is 77).

3) list.remove(Integer.valueOf(5)); // what does this one do?

    It removes the first occurrence of the specified element from this list

4)  which of the two lists performs better as the size increases?

    Performance of the LinkedList on add and remove is better than ArrayList, but worse on get and set methods.

5) what happens if you use list.remove(Integer.valueOf(77))?

   It removes the first occurrence of the specified element from this list, if it is present.
   In our case the list remains the same, since 77 has been removed from it.
